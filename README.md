## LICENSE

Simulador de destinos AGE is distributed under license Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0). More information in the link: https://creativecommons.org/licenses/by-nc-sa/4.0/

You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms.



Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional (CC BY-NC-SA 4.0)

Usted es libre de:
Compartir — copiar y redistribuir el material en cualquier medio o formato
Adaptar — remezclar, transformar y crear a partir del material
El licenciador no puede revocar estas libertades mientras cumpla con los términos de la licencia.




## INSTALACIÓN EN SERVIDOR

1.- Crear la base de datos y aplicar el script sql de creacion de tablas: sql_db/DB_Schema.sql

2.- Copiar los ficheros desde public a la carpeta public del hosting

3.- Añadir los datos de conexion de la base de datos en public/db_login.php

4.- Cambiar los datos de contacto en public/footer.php

5.- Cambiar número de opositores en variable del fichero public/get_va_data.php

6.- Cambiar email del administrador en el fichero  public/opositor_inserta.php

7.- Crear el usuario administrador, una vez creado desde phpmyadmin darle permisos de administrador tabla opositor campo es_admin = 1

