			<div class="modal-body">
				<form class="form-horizontal" id="login-form" action="./index.php?page=login" role="form"
				      method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="user">Usuario (DNI):</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="user" id="user"
							       placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="password">Password:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="password" id="password"
							       placeholder="">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<a href="./index.php?page=password_recover">Recuperar password</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">Login</button>
						</div>
					</div>
				</form>
			</div>