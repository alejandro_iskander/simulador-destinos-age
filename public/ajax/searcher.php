<?php
	/**
	 * @Project: Virtual Airlines Manager (VAM)
	 * @Author: Alejandro Garcia
	 * @Web http://virtualairlinesmanager.net
	 * Copyright (c) 2013 - 2016 Alejandro Garcia
	 * VAM is licensed under the following license:
	 *   Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
	 *   View license.txt in the root, or visit http://creativecommons.org/licenses/by-nc-sa/4.0/
	 */
?>
<?php
	session_start();
	require('../vam_pdo_connect.php');
	if (isset ($_GET['lang']))
	{
		$_SESSION['language'] = $_GET['lang'];
	}
	if (!isset($_GET['lang']) && $_SESSION['language'] == '') {
	//if (!isset($_GET['lang'])) {
		$_SESSION['language'] = "en";
	}
	if (isset($_GET['lang'])) {
		$_SESSION['language'] = $_GET['lang'];
	} elseif (!isset($_SESSION['language'])) {
		$_SESSION['languages'] = "en";
	}
	include('../classes/class_vam_mailer.php');
	include('../classes/security.php');
	include("../languages/lang_" . $_SESSION['language'] . ".php");
	$data='';
	if(isset($_POST['search_dep']))
	{
		$str_dep = $_POST['search_dep'];
		$str_dep = preg_replace("#[^0-9a-z]#i","",$str_dep);
		$str_dep = strtolower ($str_dep);
		$str_arr = $_POST['search_arr'];
		$str_arr = preg_replace("#[^0-9a-z]#i","",$str_arr);
		$str_arr = strtolower ($str_arr);
		$data='<div id="routes_public" class="table-responsive" style="height:400px;overflow:auto"><table id="routes_public" class="table table-hover">';
					$data = $data. '<thead>';
					$data = $data. '<tr><th>Código</th><th>Provincia</th><th>Localidad</th><th>Ministerio/OOAA</th><th>Puesto</th><th>Nivel</th><th>CE</th></tr>';
					$data = $data. '</thead>';
					//$sql2 = "select * from routes where lower(departure) LIKE '$str_dep%' and lower(arrival) LIKE '$str_arr%'  order by departure asc,arrival asc ";
					$sql2 = "select flight, a1.name as dep_name, a2.name as arr_name, a1.iso_country as dep_country,a2.iso_country as arr_country,route_id,departure,arrival, duration from routes r, airports a1 , airports a2 where lower(departure) LIKE :para1 and lower(arrival) LIKE :para2 and departure=a1.ident and arrival=a2.ident order by departure asc,arrival asc ";
					$sql2 = "select Provincia,Localidad,Puesto,Nivel,Codigo_Puesto,Destino,CE,Nivel from destinos where lower(Provincia) LIKE :para1 and lower(Destino) LIKE :para2";
					$result2 = get_data ($sql2,'%'.$str_dep.'%','%'.$str_arr.'%');
					foreach ($result2 as $row) {

						$data = $data. '<tr><td align="center"><b>';
						$data = $data. $row["Codigo_Puesto"].'</b></td><td>';
						$data = $data. $row["Provincia"] . '</td><td>'.$row["Localidad"].'</td><td>';
						$data = $data. $row["Destino"] . '</td><td>'.$row["Puesto"].'</td><td>';
						$data = $data. $row["Nivel"] . '</td><td>'.$row["CE"].'</td></tr>';

					}
					$data = $data.'</table></div>';
					echo $data;
	}
?>
