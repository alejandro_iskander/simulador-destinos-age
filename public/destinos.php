<?php
	$sql = "select Provincia,Localidad,Puesto,Nivel,Codigo_Puesto,Destino,CE,Nivel from destinos order by Provincia asc ";
	$result = get_data ($sql,null,null);
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;<?php echo 'Buscador de destinos'  ?></div>
				<form class="form-horizontal" id="manual-pirep-form" action="./index_vam_op.php?page=pirep_manual_pre_insert" role="form" method="post">
					<br>
					<div class="form-group">
						<label class="control-label col-sm-2" for="departure"><?php echo 'Provincia:'; ?></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" maxlength="40" name="routedeparture" id="routedeparture"
							       placeholder="<?php echo 'Provincia'; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="arrival"><?php echo 'Ministerio / OOAA:'; ?></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" maxlength="40" name="routearrival" id="routearrival"
							       placeholder="<?php echo 'Ministerio / OOAA'; ?>">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="clearfix visible-lg"></div>
<div id="no_search_result">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading"><i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp;<?php echo 'Destinos' ?></div>
				<div class="table-responsive">
				<br>
				<!-- Table -->
				<table id="routes_public" class="table table-hover">
					<?php
						echo '<thead>';
						echo '<th>Codigo</th><th>Provincia</th><th>Localidad</th><th>Ministerio/OOAA</th><th>Puesto</th><th>Nivel</th><th>CE</th></tr>';
						echo '</thead>';
						foreach ($result as $row) {	
							echo '<tr>';
							echo '<td align="center"><b>'.$row["Codigo_Puesto"].'</b></td>' ;
							echo '<td>'.$row["Provincia"].'</td>' ;
							echo '<td>'.$row["Localidad"].'</td>' ;
							echo '<td>'.$row["Destino"].'</td>' ;
							echo '<td>'.$row["Puesto"].'</td>' ;
							echo '<td>'.$row["Nivel"].'</td>' ;
							echo '<td>'.$row["CE"].'</td>' ;
							echo '</tr>';
						}
					?>
				</table>
			</div>
		</div>
	</div>
		<div class="clearfix visible-lg"></div>
	</div>
</div>
<div id="search_result">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading"><?php echo 'Destinos filtrados' ?></div>
				<div id="departureli" name="departureli" class="entry"></div>
			</div>
		</div>
		<div class="clearfix visible-lg"></div>
	</div>
</div>
