<?php
if ($user_logged== 1){
session_start();
$opositor=$_SESSION['Dni'];
  $sql = "select Provincia,count(*) as cnt from destinos d , Peticiones p where d.Codigo_Puesto=p.destino and p.posicion=1 group by Provincia order by cnt desc";
  $result = get_data ($sql,null,null);
  $sql = "select id  from Peticiones p where posicion=1";
  //echo $sql;
  $total_peticiones = get_count ($sql,null,null);
?>


<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<?php echo 'Estadísticas de peticiones para primera elección' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="estadisticas globales" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Provincia</th><th>Número de peticiones</th><th>Porcentaje</th></tr>';
            echo '</thead>';
            foreach ($result as $row) {
              $porcentaje=($row["cnt"]/$total_peticiones)*100;
              echo '<tr>';
              echo '<td>'.$row["Provincia"].'</td>' ;
              echo '<td>'.$row["cnt"].'</td>' ;
              echo '<td>'.round($porcentaje,2).' %</td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>


<?php
  $sql = "select Provincia,count(*) as cnt from destinos d , Peticiones p where d.Codigo_Puesto=p.destino group by Provincia order by cnt desc";
  $result = get_data ($sql,null,null);
  $sql = "select id  from Peticiones p ";
  $total_peticiones = get_count ($sql,null,null);
?>


<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<?php echo 'Estadísticas de peticiones globales' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="estadisticas globales" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Provincia</th><th>Número de peticiones</th><th>Porcentaje</th></tr>';
            echo '</thead>';
            foreach ($result as $row) {
              $porcentaje=($row["cnt"]/$total_peticiones)*100;
              echo '<tr>';
              echo '<td>'.$row["Provincia"].'</td>' ;
              echo '<td>'.$row["cnt"].'</td>' ;
              echo '<td>'.round($porcentaje,2).' %</td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
