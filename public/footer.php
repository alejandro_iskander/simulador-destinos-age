<br>
<div class="container-fluid">
	<hr>
	<div class="text-center center-block">
        <?php
        	if ($user_logged==1){
        		echo 'Usuario:'.$dni;
        	}
        ?>
        <br>
        <p>
        <p class="font-weight-bold"><a href="./index.php?page=manual_usuario"><i class="fa fa-graduation-cap fa-fw"></i> <?php echo 'Manual de usuario'; ?></a></p>
        <p class="font-weight-bold"><a href="./index.php?page=release_notes"><i class="fa fa-cube fa-fw"></i> <?php echo 'Cambios en el simulador'; ?></a></p>
		<p class="txt-railway"><a href="http://age-c1-2020.epizy.com/">Simulador destinos AGE [v 1.1]</a></p>
		<a href="mailto:xxxxxx@gmail.com"><i id="social" class="fa fa-envelope-square fa-3x social-em"></i></a>
		<br/>
		<br/>
	</div>
</div>
<br/>
