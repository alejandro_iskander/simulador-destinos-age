
    $(function(){
            $("#search_result").hide();
            $('#routedeparture').keyup(function(){
                var search_dep = $('#routedeparture').val();
                var search_arr = $('#routearrival').val();
                if (search_dep.length==0 && search_arr.length==0)
                {
                    $("#no_search_result").show(1000);
                    $("#search_result").hide(1000);
                }
                else
                {
                    $("#no_search_result").hide(1000);
                    $("#search_result").show(1000);
                    $.post("ajax/searcher.php",{"search_dep":search_dep,"search_arr":search_arr},function(data){
                        $('.entry').html(data); });
                }
            });
        });
    $(function(){
            $('#routearrival').keyup(function(){
                var search_dep = $('#routedeparture').val();
                var search_arr = $('#routearrival').val();
                if (search_dep.length==0 && search_arr.length==0)
                {
                    $("#no_search_result").show(1000);
                    $("#search_result").hide(1000);
                }
                else
                {
                    $("#no_search_result").hide(1000);
                    $("#search_result").show(1000);
                    $.post("ajax/searcher.php",{"search_dep":search_dep,"search_arr":search_arr},function(data){
                        $('.entry').html(data); });
                }
            });
        });
// front end js script begin
// datatables begin
$(document).ready(function(){
    $('#fleet_public').DataTable();
    $('#pilot_flights').DataTable(
    {
    columnDefs: [
         { targets: 5, type: 'numeric' },

    ],
    "order": []  
}
        );
    $('#pilots_public').DataTable();
    $('#routes_public').DataTable();
    $('#tours').DataTable();
    $('#ranks').DataTable({
        "order": []
    });
    $('#pilots_flights_per_month').DataTable();
    $('#pilots_hours_per_month').DataTable();
    $('#hub_pilot').DataTable();
    $('#hub_fleet').DataTable();
    $('#hub_routes').dataTable( {
      "searching": false
    } );
    $('#mails').DataTable();
    $('#route_select_one').DataTable();
    $('#route_select_two').DataTable();
    $('#my_bank').DataTable({
        "order": []
    });
    $('#downloads').DataTable();
    $('#report_plane_out').DataTable();
    $('#validate_flights').DataTable();
    $('#validate_jumps').DataTable();
    $('#tour_active').DataTable();
    $('#tour_inactive').DataTable();
    $('#report_pilot').DataTable({
        "order": []
    });
});
// datatables end
// validations begin
$(document).ready(function () {
    // Validation for change location form
    var validator_recover_password = $("#login-form").bootstrapValidator({
        feedbackIcons: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            user: {
                message: "Introduzca su DNI (4 cifras)",
                validators: {
                    notEmpty: {
                        message: "Introduzca su DNI (4 cifras)"
                    }
                }
            },
            password: {
                message: "Password obligatoria",
                validators: {
                    notEmpty: {
                        message: "Password obligatoria"
                    }
                }
            }
        }
    });
    // Validation for password reset form
    var validator_recover_password = $("#password-recover-form").bootstrapValidator({
        feedbackIcons: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            callsign: {
                message: "Callsign is required",
                validators: {
                    notEmpty: {
                        message: "Please provide Callsign"
                    }
                }
            },
            email: {
                message: "Email address is required",
                validators: {
                    notEmpty: {
                        message: "Campo Email es obligatorio"
                    },
                    emailAddress: {
                        message: "Email inválido"
                    }
                }
            }
        }
    });
    // Validation for register form
    var validator_register_form = $("#register-form").bootstrapValidator({
        feedbackIcons: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            name: {
                message: "Name  is required",
                validators: {
                    notEmpty: {
                        message: "Please provide a Name"
                    }
                }
            },
            surname: {
                message: "Last name is required",
                validators: {
                    notEmpty: {
                        message: "Please provide a Last name"
                    }
                }
            },
            dni: {
                message: "DNI es obligatorio",
                validators: {
                    notEmpty: {
                        message: "DNI es obligatorio"
                    }
                }
            },
            email: {
                message: "Email address is required",
                validators: {
                    notEmpty: {
                        message: "El campo Email es obligatorio"
                    },
                    emailAddress: {
                        message: "Email con formato inválido"
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: "Password es obligatoria"
                    },
                    stringLength: {
                        min: 6,
                        message: "Password debe ser al menos 6 caracteres"
                    },
                    different: {
                        field: "email",
                        message: "Email address and password can not match"
                    }
                }
            },
            password2: {
                validators: {
                    notEmpty: {
                        message: "Campo obligatorio"
                    },
                    identical: {
                        field: "password",
                        message: "Password y la confirmation deben coincidir"
                    }
                }
            },
            birthdate: {
                message: "Birthdate is required",
                validators: {
                    notEmpty: {
                        message: "Please provide a Birthdate"
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The format is dd/mm/yyyy'
                    }
                }
            },
            ivao: {
                message: "ID must be a number",
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    },
                    stringLength: {
                        message: 'Maximun 8 digits',
                        max: 8
                        }
                }
            },
            vatsim: {
                message: "ID must be a number",
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    },
                    stringLength: {
                        message: 'Maximun 8 digits',
                        max: 8
                    }
                }
            }
        }
    });

});
// validations end



$(document).ready(function () {
        // Validation for my-profile-form
        var validator_my_profile = $("#my-profile-form").bootstrapValidator({
            feedbackIcons: {
                valid: "glyphicon glyphicon-ok",
                invalid: "glyphicon glyphicon-remove",
                validating: "glyphicon glyphicon-refresh"
            },
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required"
                        },
                        stringLength: {
                            min: 6,
                            message: "Password must be 6 characters long"
                        }
                    }
                },
                password2: {
                    validators: {
                        notEmpty: {
                            message: "Campo obligatorio"
                        },
                        identical: {
                            field: "password",
                            message: "Password y confirmación deben coincidir"
                        }
                    }
                }
            }
        });



    });
