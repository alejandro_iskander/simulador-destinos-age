<?php
	session_start();
	session_unset();
	unset($_SESSION["id"]);
	unset($_SESSION["Dni"]);
	unset($_SESSION["Nombre"]);
	unset($_SESSION["Apellidos"]);
	unset($_SESSION["Correo"]);
	session_destroy();
	echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=./index.php">'
?>