<?php

if (is_logged())
	{
		$sql = 'select * from messages m, gvausers u where u.gvauser_id= m.user_from and m.status<>3 and  m.user_to= :para1 order by sentdate desc;';
		$result = get_data ($sql,$id,null);
?>
	<div class="row">
		<div class="col-md-12">
			<a href="./index_vam_op.php?page=message_create"><IMG src="images/Email-Add-32.png" BORDER=0 ALT=""></a>
			<?php echo MAILS_NEW_MESSAGE_LNK; ?><p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><IMG src="images/icons/ic_mail_outline_white_18dp_1x.png">&nbsp;<?php echo MAILS; ?></div>
				<div class="table-responsive">
				<br>
				<table id="mails" class="table table-hover">
					<?php
						echo '<thead>';
						echo '<tr><th>' . MAILS_DATE . '</th><th>' . MAILS_FROM . '</th><th>' . MAILS_SUBJECT . '</th><th>' . MAILS_ACTIONS . '</th></tr>';
						echo '</thead>';
						foreach ($result as $row) {
							echo '<tr><td>';
							echo $row["sentdate"] . '</td><td>';
							echo $row["callsign"] . '-' . $row["name"] . ' ' . $row["surname"] . '</td><td>';
							echo $row["subject"] . '</td><td>';
							echo '<a href="./index_vam_op.php?page=message_read&mail=' . $row["message_id"] . '"><IMG src="images/Mail-Open-blue-48.png" WIDTH="20" HEIGHT="20" BORDER=0 ALT=""></a>
	                  <a href="./index_vam_op.php?page=message_delete&mail=' . $row["message_id"] . '"><IMG src="images/Email-Delete-32.png" WIDTH="20" HEIGHT="20" BORDER=0 ALT=""></a></td></tr>';
						}
						echo "</table></br>";
					?>
				<table>
				</div>
			</div>
		</div>
	</div>
<?php
	}
if (is_logged())
	{
		$sql = 'select * from messages m, gvausers u where u.gvauser_id= m.user_to and m.user_from= :para1  order by sentdate desc;';
		$result = get_data ($sql,$id,null);
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><IMG src="images/icons/ic_mail_outline_white_18dp_1x.png">&nbsp;<?php echo MAILS_SEND; ?></div>
				<div class="table-responsive">
				<br>
				<table id="mails" class="table table-hover">
					<?php
						echo '<thead>';
						echo '<tr><th>' . MAILS_DATE . '</th><th>' . MAILS_TO . '</th><th>' . MAILS_SUBJECT . '</th><th>' . MAILS_ACTIONS . '</th></tr>';
						echo '</thead>';
						foreach ($result as $row) {
							echo '<tr><td>';
							echo $row["sentdate"] . '</td><td>';
							echo $row["callsign"] . '-' . $row["name"] . ' ' . $row["surname"] . '</td><td>';
							echo $row["subject"] . '</td><td>';
							echo '<a href="./index_vam_op.php?page=message_read&mail=' . $row["message_id"] . '"><IMG src="images/Mail-Open-blue-48.png" WIDTH="20" HEIGHT="20" BORDER=0 ALT=""></a></td></tr>';
						}
						echo "</table></br>";
					?>
				<table>
				</div>
			</div>
		</div>
	</div>
<?php
	}

else
	{
		include("./notgranted.php");
	}
?>
