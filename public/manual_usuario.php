
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<?php echo 'Manual de usuario' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="Release notes" class="table table-hover">
          <tr>
            <td style="text-align:left; vertical-align:middle"><b>Usuarios no registrados</b></td>
            <td>
              <li>Los usuarios no registrados no pueden solicitar destinos ni consultar el resultado de la asiganción.</li>
            </td>
          <tr>

          <tr>
            <td style="text-align:left; vertical-align:middle"><b>Búsqueda de destinos</b></td>
            <td>
              <li>Puedes buscar destinos por provincia y por ministerio/organismo.</li>
              <li>Puedes buscar destinos globalmente buscando cualquier texto en la casilla "Search".</li>
              <li>Recomendable tener el listado de destinos en una ventana/tab distinta a la que se usa para solicitar destinos.</li>
            </td>
          <tr>

          <tr>
            <td style="text-align:left; vertical-align:middle"><b>Registrarse en la web</b></td>
            <td>
              <li>Relleno los campos obligatorios.Tu usuario será los 4 dígitos del DNI que aparecen en el listado de aprobados.</li>
              <li>Nombre y apellidos no son obligatorios.</li>
              <li>Tu correo es necesario para poder recuperar la contraseña en caso de olvido.</li>
              <li>El sistema te enviará un correo confirmando el registro.</li>
            </td>
          <tr>

          <tr>
            <td style="text-align:left; vertical-align:middle"><b>Insertar solicitudes de destino</b></td>
            <td>
              <li>Cada opositor tiene una posición según el listado de aprobados.El número de destinos a solicitar debe ser tu posición + las plazas por turno de dsicapacidad.</li>
              <li>El sistema te deja solicitar sólo los destinos en base al punto anterior.</li>
              <li>Para hacer solicitud manual desde el menú superior ir a "Solicitar destino", los destinos se muestran en distintas páginas en grupo de 100 en 100.</li>
              <li>Pulsa el botón "Guardar" tras introducir los códigos de cada puesto que solicitas.</li>
              <li>Puedes editar las solicitudes previamente guardadas, regresa al formulario de peticion de destinos y edita aquellas posiciones que desees, las que no cambies el sistema dejará lo anteriormente solicitado.</li>
              <li>Puedes cargar un excel con la solicitud de tus destinos (primera columna la posición y segunda columna el código del destino). Cuando cargas un excel las solicitudes anteriores se borran.</li>
              <li>Puedes editar las solicitudes cargadas por excel, cargando un nuevo excel o editando a mano.</li>
            </td>
          <tr>   

          <tr>
            <td style="text-align:left; vertical-align:middle"><b>Alertas</b></td>
            <td>
              <li>Una vez logado en el sistema tu página principal contiene alertas interesantes para tu usuario.</li>
              <li>En tu página de usuario tendrás un link para ver el resultado de la asignación.El proceso de asiganción es automático y ejecutado cada 4 horas.</li>
              <li>En tu página de usuario puedes además exportar un excel con los destinos que has solicitado con todos los detalles.</li>
              <li>Estadísticas mostradas: Estadístics de que provincias se han solicitado por otros usuarios y estadísticas teniendo en cuenta todas las peticiones.</li>
            </td>
          <tr>           
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>

