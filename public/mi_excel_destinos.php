<?php
  require 'vendor/autoload.php';

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\IOFactory;
if ($user_logged== 1){
    // excel new from here

session_start();


  $opositor=$_SESSION['Dni'];
  $sql = "select Provincia,Localidad,Puesto,Nivel,Codigo_Puesto,d.Destino,CE,Nivel,Posicion from destinos d inner join Peticiones p on p.destino=d.Codigo_puesto where p.opositor=$opositor order by posicion asc ";

  $result = get_data ($sql,null,null);

    $destinos = Array();
    $sql = "select * from destinos order by Codigo_Puesto asc";
    $result = get_data ($sql,null,null);
    $i_dest =1;
    foreach ($result as $row) {
      $peticion->Provincia = $row["Provincia"];
      $peticion->Localidad = $row["Localidad"];
      $peticion->Destino = $row["Destino"];
      $peticion->Puesto = $row["Puesto"];
      $peticion->Codigo_Puesto = $row["Codigo_Puesto"];
      $peticion->Nivel = $row["Nivel"];
      $peticion->CE = $row["CE"];
      $json = json_encode($peticion);
      $pos = $row["Codigo_Puesto"];
      $destinos[$pos]= $json;
    }

    $sql = "select posicion, destino from Peticiones where opositor=$opositor order by posicion asc ";
    $result = get_data ($sql,null,null);
  
  $spreadsheet = new Spreadsheet();
  $sheet = $spreadsheet->getActiveSheet();
  $sheet->setCellValue('A1', 'Posicion');
  $sheet->setCellValue('B1', 'Provincia');
  $sheet->setCellValue('C1', 'Localidad');
  $sheet->setCellValue('D1', 'Puesto');
  $sheet->setCellValue('E1', 'Codigo_Puesto');
  $sheet->setCellValue('F1', 'Destino');
  $sheet->setCellValue('G1', 'Nivel');
  $sheet->setCellValue('H1', 'CE');
  $writer = new Xlsx($spreadsheet);

  $i=2;
  foreach ($result as $row) {
    $json = json_decode($destinos[$row["destino"]]);
    
    $a ='A'.$i;
    $b ='B'.$i;
    $c ='C'.$i;
    $d ='D'.$i;
    $e ='E'.$i;
    $f ='F'.$i;
    $g ='G'.$i;
    $h ='H'.$i;

    $sheet->setCellValue($a, $row["posicion"]);
    $sheet->setCellValue($b, $json->Provincia);
    $sheet->setCellValue($c, $json->Localidad);
    $sheet->setCellValue($d, $json->Puesto);
    $sheet->setCellValue($e, $json->Codigo_Puesto);
    $sheet->setCellValue($f, $json->Destino);
    $sheet->setCellValue($g, $json->Nivel);
    $sheet->setCellValue($h, $json->CE);

    $i++;
  }
  $writer->save('./excelout/Destinos_'.$opositor.'.xlsx');
?>
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Excel con destinos solicitados' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<tr>';
            echo '<td><a href="./excelout/Destinos_'.$opositor.'.xlsx">Descargar excel</a></td>';
            echo '</tr>';
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
