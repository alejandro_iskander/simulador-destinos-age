<?php
ini_set('session.use_trans_sid', false);
ini_set('session.use_cookies', true);
ini_set('session.use_only_cookies', true);
$https = false;
if(isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] != 'off') $https = true;
$dirname = rtrim(dirname($_SERVER['PHP_SELF']), '/').'/';
session_name('some_name');
session_set_cookie_params(0, $dirname, $_SERVER['HTTP_HOST'], $https, true);
ob_start();
session_start();
$logado = 0 ;
$logado =$_SESSION['Dni'];
if ($logado > 1){
  $opositor=$_SESSION['Dni'];
  $prelacion = $_SESSION['prelacion'];
  $Array_posiciones = Array();
  $Array_posiciones_libres = Array();
  $Array_posiciones_exceso = Array();
  $pos = 1;
  $pos_libre = 1;
  $pos_exceso = 1;

  if ($prelacion>$plazas-$grupo5){
    $destinos_a_pedir= $prelacion + ($plazas -$prelacion);
  }
  else{
    $destinos_a_pedir=$grupo5 + $prelacion;
  }
  //$destinos_a_pedir = ($grupo5 + $prelacion) - ($plazas);
  
  $sql = "select opositor, posicion, destino from Peticiones where opositor=$opositor order by posicion";
  //echo $sql;
  $result = get_data ($sql,null,null);
  foreach($result as $row) {
    $Array_posiciones[$pos] = $row["posicion"];
    $pos++;
  }

  for ($x = 1; $x <= $prelacion; $x++) {
    if (in_array($x, $Array_posiciones))
    {
      $ok=1;
    }
    else{
      $Array_posiciones_libres[$pos_libre]=$x;
      $pos_libre++;
    }
  }
  $destinos_faltan_pedir=$destinos_a_pedir-count($Array_posiciones);
// Duplicados

  $sql = "select opositor,destino,count(id) from Peticiones where opositor=$opositor group by opositor,destino having COUNT(*)>1";
  $duplicados = get_count ($sql,null,null);

// Personas por delante que han metido solicitudes
  $opo = Array();
  $peticio = Array();
  $solicita_delate = 0;
  $no_solicita_delante = 0;
  $i = 1;

  $sql = "select distinct(opositor) from Peticiones";
  $result = get_data ($sql,null,null);
  foreach($result as $row) {
    $peticio[$i]=$row["opositor"];
    $i++;
  }

  $sql = "select prelacion, DNI from opositor";
  $result = get_data ($sql,null,null);
  foreach($result as $row) {
    $opo[$row["DNI"]]=$row["prelacion"];
  }

  for ($i=1;$i<=sizeof($peticio);$i++){
    if ($peticio[$i]<>$opositor){
      if ($prelacion>$opo[$peticio[$i]]){
        $solicita_delate++;
      }
    }
  }

  //$solicita_delate = get_count ($sql,null,null);

  $no_solicita_delante = $prelacion - $solicita_delate -1;
// Datos del excel

  $sql = "select excel_name, excel_load from opositor where Dni=$opositor";
  $excel_name='';
  $excel_load='';
  $result = get_data ($sql,null,null);
  foreach($result as $row) {
    $excel_name=$row["excel_name"];
    if ($excel_name<>""){
      $excel_load=date_create($row["excel_load"]);
        date_add($excel_load,date_interval_create_from_date_string("6 hours"));
    }
  }

?>
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;<?php echo 'Alertas para su usuario' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="alertas" class="table table-hover">
          <tr>
            <td>Tu puesto es el <b><?php echo $prelacion ?></b></td>     
          </tr>
          <tr>
            <td>Plazas de cupo reserva personas con discapacidad <b><?php echo $grupo5 ?></b></td>     
          </tr> 
          <tr>
            <td>Has introducido <b><?php echo count($Array_posiciones) .'</b> destinos de los <b>'.$destinos_a_pedir.'</b> que debes introducir. Faltan <b>'.$destinos_faltan_pedir.'</b> destinos por solicitar'?><a href="./index.php?page=mis_destinos_sin_solicitar"><?php echo ' Ver aquí listado de destinos por solicitar.'; ?></a> </td>     
          </tr> 
          <tr>
            <td>Has introducido <b><?php echo $duplicados .'</b> destinos repetidos.'?><a href="./index.php?page=mis_duplicados"><?php echo ' Ver aquí los repetidos'; ?></a> </td>     
          </tr> 
          <tr>
            <td><b><?php echo $solicita_delate ?></b> Opositores por delante que han solicitado destinos </td>     
          </tr> 
          <tr>
            <td><b><?php echo $no_solicita_delante ?></b> Opositores por delante que NO han solicitado destinos </td>     
          </tr> 
          <tr>
            <td>Último excel cargado <b><?php echo $excel_name ?> </b>. Cargado el <b><?php  echo date_format($excel_load,'d-m-Y H:i:s') ?> </b></td>     
          </tr>
          <tr>
            <td>Excel con toda la información de los destinos cargados por tu usuario en el sistema: <a href="./index.php?page=mi_excel_destinos">Generar Excel.</a></td>     
          </tr>  
        <tr>
            <td>Ver <b>Resultado de la Asiganción</b> <a href="./index.php?page=resultado">Resultados.</a></td>     
          </tr>  
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>


<div id="cargar fichero">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp;<?php echo 'Cargar fichero de destinos (excel)' ?></div>
        <div class="table-responsive">
        
        <form action="./index.php?page=fileUploadScript" method="post" enctype="multipart/form-data">
        <!-- Table -->
        <table id="carga fichero" class="table">
          <tr>
            <td>              
                  Seleccione un fichero en formato excel (formato obligatorio xlsx)
                  <p>
                  Primera columna Posción, segunda columna código del destino.
                  <p>
                  Ejemplo : <a href="./excelout/destinos.xlsx">Excel de Ejemplo</a>
                  <p>
                  El proceso de carga puede llevar un tiempo, si se producen errores inténtalo de nuevo.<br>Si el problema persiste envia un email a <b>simuagec12020@gmail.com</b>
                  <p>
            </td>      
            <td>
              <input type="file" name="the_file" id="fileToUpload">
              <br>
              <input type="submit" name="submit" value="Cargar fichero" class="btn btn-success">
            </td>     
          </tr> 
        </table>
        </form>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
include ('./estadisticas_provincia.php');
}
  else
  {
    include("./notgranted.php");
  }
?>
