<?php
ob_start();
session_start();
if ($user_logged== 1){
session_start();
$opositor=$_SESSION['Dni'];
  $sql = "select Provincia,Localidad,Puesto,Nivel,Codigo_Puesto,d.Destino,CE,Nivel,Posicion from destinos d inner join Peticiones p on p.destino=d.Codigo_puesto where p.opositor=$opositor order by posicion asc ";
  $result = get_data ($sql,null,null);
    $destinos = Array();
    $sql = "select * from destinos order by Codigo_Puesto asc";
    $result = get_data ($sql,null,null);
    $i_dest =1;
    foreach ($result as $row) {
      $peticion->Provincia = $row["Provincia"];
      $peticion->Localidad = $row["Localidad"];
      $peticion->Destino = $row["Destino"];
      $peticion->Puesto = $row["Puesto"];
      $peticion->Codigo_Puesto = $row["Codigo_Puesto"];
      $peticion->Nivel = $row["Nivel"];
      $peticion->CE = $row["CE"];
      $json = json_encode($peticion);
      $pos = $row["Codigo_Puesto"];
      $destinos[$pos]= $json;
     // $destinos[$i_dest]=$json;
      //$i_dest ++;
    }
    //print_r($destinos);
    $sql = "select posicion, destino from Peticiones where opositor=$opositor order by posicion asc ";
    $result = get_data ($sql,null,null);
?>


<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp;<?php echo 'Destinos solicitados' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Posicion</th><th>Provincia</th><th>Localidad</th><th>Ministerio/OOAA</th><th>Puesto</th><th>Código Puesto</th><th>Nivel</th><th>CE</th></tr>';
            echo '</thead>';
            foreach ($result as $row) {  
              $json = json_decode($destinos[$row["destino"]]);
              echo '<tr>';
              echo '<td>'.$row["posicion"].'</td>' ;
              echo '<td>'.$json->Provincia.'</td>' ;
              echo '<td>'.$json->Localidad.'</td>' ;
              echo '<td>'.$json->Destino.'</td>' ;
              echo '<td>'.$json->Puesto.'</td>' ;
              echo '<td>'.$json->Codigo_Puesto.'</td>' ;
              echo '<td>'.$json->Nivel.'</td>' ;
              echo '<td>'.$json->CE.'</td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
