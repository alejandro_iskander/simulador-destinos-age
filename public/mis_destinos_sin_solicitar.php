<?php
if ($user_logged== 1){
  session_start();
  $opositor=$_SESSION['Dni'];
  $prelacion = $_SESSION['prelacion'];
  $Array_posiciones = Array();
  $Array_posiciones_libres = Array();
  $Array_posiciones_exceso = Array();
  $pos = 1;
  $pos_libre = 1;
  $pos_exceso = 1;
  $destinos_a_pedir=$grupo5 + $prelacion;


  if ($destinos_a_pedir>$plazas-$grupo5){
    $destinos_a_pedir= $prelacion + ($plazas -$prelacion);
  }
  else{
    $destinos_a_pedir=$grupo5 + $prelacion;
  }



  
  $sql = "select opositor, posicion, destino from Peticiones where opositor=$opositor order by posicion";
  //echo $sql;
  $result = get_data ($sql,null,null);
  foreach($result as $row) {
    $Array_posiciones[$pos] = $row["posicion"];
    $pos++;
  }

  for ($x = 1; $x <= $destinos_a_pedir; $x++) {
    if (in_array($x, $Array_posiciones))
    {
      $ok=1;
    }
    else{
      $Array_posiciones_libres[$pos_libre]=$x;
      $pos_libre++;
    }
  }
  $destinos_faltan_pedir=$destinos_a_pedir-count($Array_posiciones);
?>
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Destinos sin solicitar' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Posición</th></tr>';
            echo '</thead>';
            for ($x = 1; $x <= count($Array_posiciones_libres); $x++) {  
              echo '<tr>';
              echo '<td>'.$Array_posiciones_libres[$x].'</td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
