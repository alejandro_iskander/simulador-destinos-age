<?php
if ($user_logged== 1){
session_start();
    $opositor=$_SESSION['Dni'];
    $destinos = Array();
    $sql = "select * from destinos order by Codigo_Puesto asc";
    $result = get_data ($sql,null,null);
    $i_dest =1;
    foreach ($result as $row) {
      $peticion->Provincia = $row["Provincia"];
      $peticion->Localidad = $row["Localidad"];
      $peticion->Destino = $row["Destino"];
      $peticion->Puesto = $row["Puesto"];
      $peticion->Codigo_Puesto = $row["Codigo_Puesto"];
      $peticion->Nivel = $row["Nivel"];
      $peticion->CE = $row["CE"];
      $json = json_encode($peticion);
      $destinos[$row["Codigo_Puesto"]]=$json;
      $i_dest ++;
    }
  $sql ="select opositor,destino,count(id) from Peticiones where opositor=$opositor group by opositor,destino having COUNT(*)>1";
  $result = get_data ($sql,null,null);
  $i_duplicados=1;
  $duplicados = Array();
  foreach ($result as $row) {
    $duplicados[$row["destino"]] = $row["destino"];
  }

  $mis_peti=Array ();
  $sql = "select * from Peticiones where opositor=$opositor order by destino asc,posicion asc";
  $result = get_data ($sql,null,null);
  $i_peticion =1;
  foreach ($result as $row) {
    $peti->posicion = $row["posicion"];
    $peti->destino = $row["destino"];
    $json = json_encode($peti);
    $mis_peti[$row["posicion"]]=$json;
    $i_peticion ++;
    };  
?>
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp;<?php echo 'Destinos solicitados más de una vez' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Posición</th><th>Provincia</th><th>Localidad</th><th>Ministerio/OOAA</th><th>Puesto</th><th>Código Puesto</th><th>Nivel</th><th>CE</th></tr>';
            echo '</thead>';
            for ($x = 1; $x <= 2029; $x++) {
              $json = json_decode($mis_peti[$x]);
              if ($json->destino!=""){
                if ($json->destino==$duplicados[$json->destino]){  
                    $json_destino = json_decode($destinos[$json->destino]);
                    echo '<tr>';
                    echo '<td align="center">'.$json->posicion.'</td>' ;
                    echo '<td>'.$json_destino->Provincia.'</td>' ;
                    echo '<td>'.$json_destino->Localidad.'</td>' ;
                    echo '<td>'.$json_destino->Destino.'</td>' ;
                    echo '<td>'.$json_destino->Puesto.'</td>' ;
                    echo '<td align="center">'.$json_destino->Codigo_Puesto.'</td>' ;
                    echo '<td align="center">'.$json_destino->Nivel.'</td>' ;
                    echo '<td>'.$json_destino->CE.'</td>' ;
                    echo '</tr>';
                }
              }
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
