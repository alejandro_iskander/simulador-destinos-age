<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require './vendor/PHPMailer/src/Exception.php';
	require './vendor/PHPMailer/src/PHPMailer.php';
	require './vendor/PHPMailer/src/SMTP.php';
	$nombre = $_POST['nombre'];
	$apellidos = $_POST['apellidos'];
	$dni = $_POST['dni'];
	$correo = $_POST['email'];
	$pass = $_POST['password'];
	$captcha = $_POST['captcha'];
	$captchahidden = $_POST['captchahidden'];
	if ($captcha!= $captchahidden)
	{
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo 'Error'; ?></div>
				<div class="alert alert-danger" role="alert"><?php echo 'Captcha no válido'; ?></div>
			<div>
		<div>
	<div>
	<?php
	}
	else
	{
		$sql = "SELECT * from opositor WHERE Dni=:para1";
		$existentuser = get_count ($sql,$dni,null);
		if ($existentuser > 0)
		{
?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><?php echo 'Error'; ?></div>
						<div class="alert alert-danger" role="alert"><?php echo 'Usuario ya registrado previamente'; ?></div>
					<div>
				<div>
			<div>
<?php
		}
		else
		{
			if ($_POST["password"])
			{
				$encryptpassword = password_hash($_POST['password'], PASSWORD_DEFAULT);
				echo '<br>';
				//$sql1 = "insert into gvausers (register_date,activation,name,surname,callsign,email,new_password,ivaovid,hub_id,country,city,reg_comments,birth_date,vatsimid,language,pass_secured,password)
                //    values (now(),0,'$name','$surname','_NEW_','$email','$encryptpassword','$ivao','$hub_id','$country','$city','$notes','$birthday','$vatsim','$language',1,NULL);";
				$data_name = Array('Nombre','Apellidos','activo','new_password','Correo','Dni','Pass');
				$data_para = Array($nombre,$apellidos,0,$encryptpassword,$correo,$dni,$pass);
				$table = 'opositor';
				ins_data($table,$data_name,$data_para);
			}
			// Enviar email a los admin


				$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
				try {
				    //Server settings
				    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'xxxxx';                 // SMTP username
				    $mail->Password = 'xxxxx';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

				    //Recipients
				    $mail->setFrom('xxxxxx@gmail.com', 'Simulador Destinos C1 AGE');
				    $mail->addAddress('xxxxx@gmail.com');     // Add a recipient
				    //$mail->addAddress('ellen@example.com');               // Name is optional
				    //$mail->addReplyTo('info@example.com', 'Information');
				    //$mail->addCC('cc@example.com');
				    //$mail->addBCC('bcc@example.com');

				    //Attachments
				    //$mail->addAttachment('./destinos.xlsx');         // Add attachments
				    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->Subject = 'Nuevo usuario '.$nombre .' ' .$apellidos;
				    $mail->Body    = 'Un nuevo usuario se ha registrado.<p>Nombre: '.$nombre.'<p>Apellidos: '.$apellidos.'<p>DNI: '.$dni.'<p><br>Un saludo.';
				   // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				    $mail->send();
				    //echo 'Message has been sent';
				} catch (Exception $e) {
				    echo 'Message could not be sent.';
				    echo 'Mailer Error: ' . $mail->ErrorInfo;
				}


			// Enviar email a opositor


				$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
				try {
				    //Server settings
				    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'xxxxxx@gmail.com';                 // SMTP username
				    $mail->Password = 'xxxxx';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

				    //Recipients
				    $mail->setFrom('simuagec12020@gmail.com', 'Simulador Destinos C1 AGE');
				    //$mail->addAddress('xxxxxx@gmail.com');     // Add a recipient
				    $mail->addAddress($correo); 
                    //$mail->addAddress('ellen@example.com');               // Name is optional
				    //$mail->addReplyTo('info@example.com', 'Information');
				    //$mail->addCC('cc@example.com');
				    //$mail->addBCC('bcc@example.com');

				    //Attachments
				    //$mail->addAttachment('./destinos.xlsx');         // Add attachments
				    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->Subject = 'Registro en Simulador Destinos AGE '.$nombre .' ' .$apellidos;
				    $mail->Body    = 'Hola!<p>Su usuario ha sido correctamente creado.<p>Para logarse en el sistema debe usar su DNI como usuario<p>Nombre: '.$nombre.'<p>Apellidos: '.$apellidos.'<p>DNI: '.$dni.'<p><br>Un saludo.';
				   // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				    $mail->send();
				    //echo 'Message has been sent';
				} catch (Exception $e) {
				    echo 'Message could not be sent.';
				    echo 'Mailer Error: ' . $mail->ErrorInfo;
				}
?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><?php echo 'Registro'; ?></div>
						<div class="alert alert-success" role="alert"><?php echo 'Usuario creado correctamente'; ?></div>
					<div>
				<div>
			<div>
<?php
		}
	}
?>
