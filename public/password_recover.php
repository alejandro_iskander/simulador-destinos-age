<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading"><?php echo 'Recuperar Password' ?></div>
			<p>

			<form class="form-horizontal" id="password-recover-form" action="./index.php?page=password_recordar"
			      role="form" method="post">
				<div class="form-group">
					<label class="control-label col-sm-2"
					       for="dni"><?php echo 'Usuario/DNI:' ?></label>

					<div class="col-md-4">
						<input type="text" class="form-control" name="dni" id="dni"
						       placeholder="<?php  ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2" for="email"><?php echo 'Email:' ?></label>

					<div class="col-md-4">
						<input type="email" class="form-control" name="email" id="email"
						       placeholder="<?php  ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-2">
						<button type="submit" class="btn btn-primary"><?php echo 'Recuperar la password' ?></button>
					</div>
				</div>
			</form>

		</div>
	</div>

	<div class="clearfix visible-lg"></div>
</div>
