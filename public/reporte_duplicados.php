<?php
session_start();
    $opositor=$_SESSION['Dni'];
$sql_hub = "select * from hubs h inner join airports a on a.ident = h.hub where hub_id=:para1";
  $sql = "select Provincia,Localidad,Puesto,Nivel,Codigo_Puesto,d.Destino,CE,Nivel,Posicion from destinos d inner join Peticiones p on p.destino=d.Codigo_puesto where opositor=$opositor order by posicion asc ";
  $result = get_data ($sql,null,null);
  //session_start(); 
  //print_r ($_SESSION);
  //echo "<p>";

?>


<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Destinos solicitados' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Posicion</th><th>Provincia</th><th>Localidad</th><th>Ministerio/OOAA</th><th>Puesto</th><th>Codigo</th><th>CE</th></tr>';
            echo '</thead>';
            foreach ($result as $row) {

  
              echo '<tr>';
              echo '<td>'.$row["Posicion"].'</td>' ;
              echo '<td>'.$row["Provincia"].'</td>' ;
              echo '<td>'.$row["Localidad"].'</td>' ;
              echo '<td>'.$row["Destino"].'</td>' ;
              echo '<td>'.$row["Puesto"].'</td>' ;
              echo '<td>'.$row["Codigo_Puesto"].'</td>' ;
              echo '<td>'.$row["CE"].'</td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<div id="search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><?php echo 'Destinos filtrados' ?></div>
        <div id="departureli" name="departureli" class="entry"></div>
      </div>
    </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
