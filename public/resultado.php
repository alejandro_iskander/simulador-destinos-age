<?php
if ($user_logged== 1){
//include ('./vam_pdo_connect.php');
include ('./asignar.php');
session_start();
  $opositor=$_SESSION['Dni'];
  $sql = "select Dni,prelacion,nombre,apellidos,eleccion_opositor, localidad, provincia, puesto,Codigo_Puesto,Nivel,CE, puesto, nivel, d.destino from (select o.Dni, o.prelacion, o.nombre , o.apellidos, a.eleccion_opositor, destino from opositor o , Asignacion a where o.Dni=a.opositor) as T, destinos d where T.destino=d.Codigo_Puesto order by T.prelacion ASC";
  $result = get_data ($sql,null,null);
  
?>
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Destinos Asignados' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="destinos" class="table table-hover">
          <?php
            echo '<thead>';
            echo '<tr><th>Posición</th><th>DNI</th><th>Nombre</th><th>Provincia</th><th>Localidad</th><th>Cod.</th><th>Destino</th><th>Puesto</th><th>Nivel</th><th>CE</th><th>Elección</th></tr>';
            echo '</thead>';
            foreach ($result as $row) {          
              echo '<tr>';
              echo '<td align="center">'.$row["prelacion"].'</td>' ;
              echo '<td>'.$row["Dni"].'</td>' ;
              echo '<td>'.$row["nombre"].' '.$row["apellidos"].'</td>' ;
              echo '<td>'.$row["provincia"].'</td>' ;
              echo '<td>'.$row["localidad"].'</td>' ;
              echo '<td align="center">'.$row["Codigo_Puesto"].'</td>' ;
              echo '<td>'.$row["destino"].'</td>' ;
              echo '<td>'.$row["puesto"].'</td>' ;
              echo '<td>'.$row["nivel"].'</td>' ;
              echo '<td>'.$row["CE"].'</td>' ;
              echo '<td align="center"><p class="p-3 mb-2 bg-success text-white">'.$row["eleccion_opositor"].'</p></td>' ;
              echo '</tr>';
            }
          ?>
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>
