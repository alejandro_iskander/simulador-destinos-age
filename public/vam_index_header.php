<!DOCTYPE html>
<html lang="es">
<head>
	<title>Simulador Destinos AGE</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/favicon.ico" >
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-datetimepicker.min.css"/>
	<script src="js/bootstrapValidator.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/moment-with-locales.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/jquery.confirm.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
	<!-- data tables plugins -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/numeric-comma.js"></script>
	<script src="js/vam.js" type="text/javascript"></script>

</head>
<body>
<?php
	$casillas_a_mostrar = $prelacion + $grupo5;
?>
<nav class="navbar navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
			        aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./index.php">Simulador de Destinos AGE</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">

				<?php
					if ($user_logged==0){
					?>	
						<li><a href="./index.php"><i class="fa fa-home fa-fw"></i> <?php echo 'Inicio'; ?></a></li>
					<?php
					}
					else{
					?>
						<li><a href="./index.php"><i class="fa fa-user-circle-o fa-fw"></i> <?php echo 'Usuario'; ?></a></li>
				<?php
					}
				?>



				<li><a href="./index.php?page=destinos"><i class="fa fa-globe fa-fw"></i> <?php echo 'Destinos'; ?></a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-pencil-square-o"></i> <?php echo 'Solicitar Destino'; ?>
						<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="./index.php?page=solicitar_destinos_1_100"><i class="fa fa-pencil-square-o"></i> <?php echo '1-100'; ?></a></li>
					<?php
						if ($casillas_a_mostrar>=101){
					?>
							<li><a href="./index.php?page=solicitar_destinos_101_200"><i class="fa fa-pencil-square-o"></i> <?php echo '101-200'; ?></a></li>
					<?		
						}
					?>

					<?php
						if ($casillas_a_mostrar>=201){
					?>
						<li><a href="./index.php?page=solicitar_destinos_201_300"><i class="fa fa-pencil-square-o"></i> <?php echo '201-300'; ?></a></li>					<?		
						}
					?>
					

					<?php
						if ($casillas_a_mostrar>=301){
					?>
						<li><a href="./index.php?page=solicitar_destinos_301_400"><i class="fa fa-pencil-square-o"></i> <?php echo '301-400'; ?></a></li>
					<?php
						}
					?>


					<?php
						if ($casillas_a_mostrar>=401){
					?>
						<li><a href="./index.php?page=solicitar_destinos_401_500"><i class="fa fa-pencil-square-o"></i> <?php echo '401-500'; ?></a></li>
					<?php
						}
					?>
	
					
					<?php
						if ($casillas_a_mostrar>=501){
					?>
						<li><a href="./index.php?page=solicitar_destinos_501_600"><i class="fa fa-pencil-square-o"></i> <?php echo '501-600'; ?></a></li>
					<?php
						}
					?>

					<?php
						if ($casillas_a_mostrar>=601){
					?>
						<li><a href="./index.php?page=solicitar_destinos_601_700"><i class="fa fa-pencil-square-o"></i> <?php echo '601-700'; ?></a></li>
					<?php
						}
					?>

				
					<?php
						if ($casillas_a_mostrar>=701){
					?>
						<li><a href="./index.php?page=solicitar_destinos_701_800"><i class="fa fa-pencil-square-o"></i> <?php echo '701-800'; ?></a></li>
					<?php
						}
					?>	
					

					<?php
						if ($casillas_a_mostrar>=801){
					?>
						<li><a href="./index.php?page=solicitar_destinos_801_900"><i class="fa fa-pencil-square-o"></i> <?php echo '801-900'; ?></a></li>
					<?php
						}
					?>

					<?php
						if ($casillas_a_mostrar>=901){
					?>
						<li><a href="./index.php?page=solicitar_destinos_901_1000"><i class="fa fa-pencil-square-o"></i> <?php echo '901-1000'; ?></a></li>
					<?php
						}
					?>
					

					<?php
						if ($casillas_a_mostrar>=1001){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1001_1100"><i class="fa fa-pencil-square-o"></i> <?php echo '1001-1100'; ?></a></li>
					<?php
						}
					?>
		
					<?php
						if ($casillas_a_mostrar>=1101){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1101_1200"><i class="fa fa-pencil-square-o"></i> <?php echo '1101-1200'; ?></a></li>
					<?php
						}
					?>		
					
					<?php
						if ($casillas_a_mostrar>=1201){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1201_1300"><i class="fa fa-pencil-square-o"></i> <?php echo '1201-1300'; ?></a></li>
					<?php
						}
					?>					
					

					<?php
						if ($casillas_a_mostrar>=1301){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1301_1400"><i class="fa fa-pencil-square-o"></i> <?php echo '1301-1400'; ?></a></li>
					<?php
						}
					?>
					
					<?php
						if ($casillas_a_mostrar>=1401){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1401_1500"><i class="fa fa-pencil-square-o"></i> <?php echo '1401-1500'; ?></a></li>
					<?php
						}
					?>	

					<?php
						if ($casillas_a_mostrar>=1501){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1501_1600"><i class="fa fa-pencil-square-o"></i> <?php echo '1501-1600'; ?></a></li>
					<?php
						}
					?>					
					
					<?php
						if ($casillas_a_mostrar>=1601){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1601_1700"><i class="fa fa-pencil-square-o"></i> <?php echo '1601-1700'; ?></a></li>
					<?php
						}
					?>	


					<?php
						if ($casillas_a_mostrar>=1701){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1701_1800"><i class="fa fa-pencil-square-o"></i> <?php echo '1701-1800'; ?></a></li>
					<?php
						}
					?>	
	

					<?php
						if ($casillas_a_mostrar>=1801){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1801_1900"><i class="fa fa-pencil-square-o"></i> <?php echo '1801-1900'; ?></a></li>
					<?php
						}
					?>	


					<?php
						if ($casillas_a_mostrar>=1901){
					?>
						<li><a href="./index.php?page=solicitar_destinos_1901_2000"><i class="fa fa-pencil-square-o"></i> <?php echo '1901-2000'; ?></a></li>
					<?php
						}
					?>	


					<?php
						if ($casillas_a_mostrar>=2001){
					?>
						<li><a href="./index.php?page=solicitar_destinos_2001_2100"><i class="fa fa-pencil-square-o"></i> <?php echo '2001-2100'; ?></a></li>
					<?php
						}
					?>	
	


					<?php
						if ($casillas_a_mostrar>=2101){
					?>
						<li><a href="./index.php?page=solicitar_destinos_2101_2200"><i class="fa fa-pencil-square-o"></i> <?php echo '2101-2200'; ?></a></li>
					<?php
						}
					?>						
					
					
					
				</ul>
				<li><a href="./index.php?page=mis_destinos"><i class="fa fa-sort-amount-asc fa-fw"></i> <?php echo 'Mis Destinos'; ?></a></li>
				<li><a href="./index.php?page=mis_duplicados"><i class="fa fa-window-restore fa-fw"></i> <?php echo 'Repetidos'; ?></a></li>
				
				<?php
				if ($es_admin==1) {
				?>

				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cog"></i> <?php echo 'Admin'; ?>
						<span class="caret"></span></a>
					<ul class="dropdown-menu">
					<li><a href="./index.php?page=adm_opositores"><i class="fa fa-user"></i> <?php echo 'Opositores'; ?></a></li>
					<li><a href="./index.php?page=asignar"><i class="fa fa-database"></i> <?php echo 'Asignar'; ?></a></li>
					<li><a href="./index.php?page=asignar_fake"><i class="fa fa-database"></i> <?php echo 'Fake petiones'; ?></a></li>
					<li><a href="./index.php?page=resultado"><i class="fa fa-thumbs-up"></i> <?php echo 'Resultado'; ?></a></li>
				</ul>
			</ul>
			<?php 
				}
				if ($user_logged==0) {
				?>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="./index.php?page=opositor_registro"><span class="glyphicon glyphicon-user"></span> &nbsp;<?php echo 'Registrarse'; ?></a></li>
					<li><a href="./index.php?page=acceso"><span class="glyphicon glyphicon-user"></span> &nbsp;<?php echo 'Login'; ?></a></li>
				</ul>
			<?php
			}
			else{
				
				?>
				<ul class="nav navbar-nav navbar-right" >
					<li><a href = "./index.php?page=logout" ><span class="glyphicon glyphicon-log-out" ></span >&nbsp;<?php echo 'Salir' ; ?></a ></li>
				</ul>
			<?php
			}
			?>
		</div>
	</div>
</nav>

<!-- Carousel for images -->
<div class="container">
<div class="row">
	<div class="col-md-12">

	</div>
</div>

<br>

</body>


</html>
