

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_25749661_C1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Asignacion`
--

CREATE TABLE `Asignacion` (
  `id` int(11) NOT NULL,
  `opositor` int(11) NOT NULL,
  `destino` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `eleccion_opositor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `destinos`
--

CREATE TABLE `destinos` (
  `id` int(11) NOT NULL,
  `Destino` varchar(300) NOT NULL,
  `Provincia` varchar(100) NOT NULL,
  `Localidad` varchar(200) NOT NULL,
  `Puesto` varchar(300) NOT NULL,
  `Codigo_Puesto` varchar(100) NOT NULL,
  `Nivel` varchar(11) NOT NULL,
  `CE` varchar(50) NOT NULL,
  `ocupado` int(11) NOT NULL DEFAULT '0',
  `opositor` int(11) NOT NULL,
  `seleccion_opositor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `opositor`
--

CREATE TABLE `opositor` (
  `id` int(11) NOT NULL,
  `prelacion` int(11) DEFAULT NULL,
  `Dni` varchar(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Apellidos` varchar(200) NOT NULL,
  `Pass` varchar(50) NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '0',
  `new_password` varchar(255) NOT NULL,
  `es_admin` int(11) DEFAULT '0',
  `ultimo_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `excel_name` varchar(255) NOT NULL,
  `excel_load` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `Peticiones`
--

CREATE TABLE `Peticiones` (
  `id` int(11) NOT NULL,
  `opositor` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  `destino` varchar(100) NOT NULL,
  `fecha_solicitud` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prelacion` int(11) DEFAULT NULL,
  `dni` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Asignacion`
--
ALTER TABLE `Asignacion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opositor`
--
ALTER TABLE `opositor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Peticiones`
--
ALTER TABLE `Peticiones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Asignacion`
--
ALTER TABLE `Asignacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opositor`
--
ALTER TABLE `opositor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2507;

--
-- AUTO_INCREMENT for table `Peticiones`
--
ALTER TABLE `Peticiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
